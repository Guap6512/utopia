/obj/machinery
	var/req_password = 0

/obj/machinery/proc/receive_command(var/command = "")
	if(!command)
		return
	if(!is_operational())
		return

	var/list/PacketParts = GetPacketContentUppercased(command)
	if(PacketParts.len < 2)
		return 0

	if(req_password && !check_password(PacketParts[1]))
		return

	execute_command(PacketParts)

/obj/machinery/proc/execute_command(var/list/PacketParts)


/obj/machinery/proc/check_network_access()
	return 1

/obj/machinery/proc/check_password(var/password)
	if(!src.req_access) //no requirements
		return 1
	if(!istype(src.req_access, /list)) //something's very wrong
		return 1

	var/list/L = src.req_access
	if(!L.len) //no requirements
		return 1

	for(var/req in src.req_access)
		if(accesspasswords["[req]"] == password) //doesn't have this access
			return 1
	return 0
/*
/proc/StripNetworkPacketHeader(message as text)
	var/list/messagelist = dd_text2list(message," ",null)
	if(uppertext(messagelist[2]) == "MULTI")
		return copytext(message,15 + (messagelist[3] == "***" ? 0 : 3),0) //"123 MULTI 456789 COMMAND" => "COMMAND"
	else																 //"123 MULTI *** COMMAND" => "COMMAND"
		return copytext(message,10) //"123 4567 COMMAND" => "COMMAND"


/proc/PacketHasStringAtIndex(var/list/message, var/index, var/word)
	if(message.len >= index)
		if(message[index] == word)
			return 1
	return 0
*/

/proc/GetPacketContentUppercased(var/message as text)
	return dd_text2list(uppertext(message), " ", null)

mob/verb/strip_test()

	var/message = input("Choose a text to format", "text")
	src << GetPacketContentUppercased(message)