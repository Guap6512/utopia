#include "pods_controller.dm"

var/vessel_name = ""//"NSV Luna"
var/vessel_type = "station"
var/list/vessel_z = list(1,2)
var/asteroid_z = 8
var/centcomm_z = 4
var/evac_type = "pods"
var/evac_name = "shuttle"
var/list/accessable_z_levels = list("1" = 30, "2" = 30, "3" = 20, "5" = 20)


/obj/effect/mapinfo/ship/luna
	name = "NSS Utopia"
	shipname = "NSS Utopia"
	obj_type = /obj/effect/map/ship/luna
	mapx = 11
	mapy = 10

/obj/effect/map/ship/luna
	shipname = "NSS Utopia"
	name = "NSS Utopia"
	desc = "Space faring vessel."
	icon = 'maps/overmap/bearcat/bearcat.dmi'
	icon_state = "luna"